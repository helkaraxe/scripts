#!/bin/bash

#    Script for splitting input video file to several smaller video files.
#
#    Copyight (C) 2022 - Stanislav Grinkov
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

usage() {
cat <<EOF

  This script is used to split any input video file to several files
    output file format is fixed to '.mp4'
      
  usage $0 <video_file> <time_pairs_file>
    video_file - is any format which is readable by ffmpeg
    time_pairs_file - file with time pairs in duration format
      00:00:00 00:01:01
      00:05:00 00:10:00
      ...

  To merge video files use "ffmpeg -f concat -i merge_file output.ext"
    merge_file file format:
    file <input.ext>
    file <input.ext>
    ...

EOF
}

if [ $# -le 1 ]; then
  usage
  exit 0
fi

set -eu

is_exist() {
  if [ ! -f "$1" ]; then
    echo "Input video file $1 does not exist!"
    exit 1
  fi

  if [ ! -f "$2" ]; then
    echo "Input time pairs file $2 does not exist!"
    exit 1
  fi
}

merge_file="$1.merge"
split_sh_file="run_to_split_me.sh"
time_pairs_file="$2"

cleanup() {
  if [ -f "$split_sh_file" ]; then
    rm "$split_sh_file"
  fi

  if [ -f "$merge_file" ]; then
    rm "$merge_file"
  fi
}

do_work() {
  while IFS=$'\n' read -r time_pair
  do
    param="-ss "
    param_sep=" -to "

    part_file=""
    part_file_sep="-"

    IFS=$' '
    for v in $time_pair; do
      param="$param""$v""$param_sep"
      param_sep=""
      
      part_file="$part_file""$v""$part_file_sep"
      part_file_sep=""
    done
    part_file=$(tr -d ':' <<<"$part_file")
    part_file="$part_file".mp4
    command="ffmpeg -i $1 -an $param $part_file"
	  echo "$command"
	  
    echo "$command" >> "$split_sh_file"
    echo "file $part_file" >> "$merge_file"
	  
  done < "$time_pairs_file"
  chmod u+x "$split_sh_file"
  ./"$split_sh_file"
}



is_exist "$1" "$2"
cleanup
do_work "$1" "$2"

#!/bin/bash

#    Script for merging several video files into one. Was the part of the toolbox for making videos for IG
#    
#    Copyight (C) 2022 - Stanislav Grinkov
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

 COLOR_PRIMARIES="bt709"
 COLOR_TRC="bt709"              # usual transfer for modern bt470bg but please check using command above
 COLOR_SPACE="bt709"
 COLOR_MATRIX="bt709"

 COLOR_MATRIX_VF="scale=out_color_matrix=${COLOR_MATRIX},"
 COLOR_ATTRS="-color_primaries ${COLOR_PRIMARIES} -color_trc ${COLOR_TRC} -colorspace ${COLOR_SPACE} ${CHROMA_LOCATION} ${COLOR_RANGE}"

 CODEC_COLOR_PARAMS="-x264opts colorprim=${COLOR_PRIMARIES}:transfer=${COLOR_TRC}:colormatrix=${COLOR_MATRIX}"

 CODEC_ATTRS="-codec:v libx264 ${CODEC_COLOR_PARAMS} -pix_fmt ${PIXEL_FORMAT} -preset:v slow -profile:v baseline -crf 20"

 EXPERIMENTAL_ATTRS="-movflags +write_colr"  # "" # -color_range tv -level 8
 EXPERIMENTAL_BITSTREAM_METADATA_ATTRS="" # "-bsf:v h264_metadata=video_full_range_flag=0"

 OUTPUT_FILE="e1.mp4"
if [[ -n "$2" ]]; then
  OUTPUT_FILE="$2"
fi

TEMP_FILE="temp_${OUTPUT_FILE}"

set -euo pipefail
# ${CODEC_ATTRS}
 ffmpeg -f concat -i "$1"  -c:v copy  ${COLOR_ATTRS} ${EXPERIMENTAL_ATTRS} "$TEMP_FILE"
 ffmpeg -i "${TEMP_FILE}" -metadata:s:0 rotate=180 ${COLOR_ATTRS} ${EXPERIMENTAL_BITSTREAM_METADATA_ATTRS} -c copy -y "${OUTPUT_FILE}"
 rm -f "${TEMP_FILE}"

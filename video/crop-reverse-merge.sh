#!/bin/sh

#    Source code of various video editing operations using ffmpeg. Was the part of the toolbox for making videos for IG.
#    Copyight (C) 2022 - Stanislav Grinkov
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

# This script was used to process videos for posting to IG (in 2015-2017). Now it is unmaintained.
# How to use it. If you want to use something, comment/uncomment required functionality

make_clip() {
 START="$1"
 END="$2"
 EXP="${3:-}"
 DURATION=$(( END - START ))
 SPEEDUP=""
 if [ -n "$EXP" ]; then
   SPEEDUP="*(${EXP}/${DURATION})"
 fi

 echo "trim=start=${START}:end=${END},setpts=(PTS-STARTPTS)${SPEEDUP}"
}

concat_clips() {
  STREAM_COUNT=0
  STREAMS=""
  CLIPS=""
  for clip in "$@"; do
    STREAM_ID="[p${STREAM_COUNT}]"
    STREAMS="${STREAMS}${STREAM_ID}"
    CLIPS="${CLIPS}${STREAM_ID}${clip}${STREAM_ID};"
    STREAM_COUNT=$(( STREAM_COUNT + 1 ))
  done
  echo "split=${STREAM_COUNT}${STREAMS}; ${CLIPS} ${STREAMS}concat=n=${STREAM_COUNT},"
}

# overlay one on top of other
# ffmpeg -i temp_p1.mp4 -i temp_p2.mp4 -filter_complex "nullsrc=size=1080x1920 [bg]; [bg][0:v]overlay=shortest=1[top];[top][1:v]overlay=shortest=1:y=960[out];[out]format=yuvj420p[out]" -map "[out]" -codec:v libx264 -y merged.mp4


# CLIP_1=$(make_clip 20 30 7)
# CLIP_2=$(make_clip 30 210 30)
# CLIP_3=$(make_clip 210 230)
# MAKE_CLIPS=$(concat_clips "${CLIP_1}" "${CLIP_2}" "${CLIP_3}")

# TRIM_START="0.0"
# TRIM_DURATION=":duration=6.2"
# TRIM_END=":end=7"

# ALL_TRIM="trim=start=${TRIM_START}${TRIM_DURATION}${TRIM_END},setpts=PTS-STARTPTS,"

# unused at moment
# DESIRED_DURATION=10
# CURRENT_DURATION=15
# PRE_FIX_DURATION="setpts=(PTS-STARTPTS)*(${DESIRED_DURATION}/${CURRENT_DURATION})"

# FPS_30="fps=30,"

 CROP_SIZE=2160    # for 4k
 CROP_SIZE=1080    # for 1920x1080  # comment out to disable
 CROP_SHIFT_X="0"
 CROP_SHIFT_Y="0"

 CROP="crop=${CROP_SIZE}:${CROP_SIZE}:(iw-ow) / 2 + ${CROP_SHIFT_X}:(ih-oh) / 2 + ${CROP_SHIFT_Y},"


# TRANSP="transpose=cclock,"
# TRANSP="transpose=cclock_flip,"
# TRANSP="transpose=clock,"
# TRANSP="transpose=clock_flip,"

 TRANSP="transpose=cclock,transpose=cclock,"                    # 2 top right normal
# TRANSP="transpose=cclock,transpose=cclock_flip,"               # 3
# TRANSP="transpose=cclock_flip,transpose=cclock,"               # 4 top right flipped
# TRANSP="transpose=cclock_flip,transpose=cclock_flip,"          # 5

# VFLIP="vflip,"
# HFLIP="hflip,"

# black point
 LEVEL_R_BP=10
 LEVEL_G_BP=10
 LEVEL_B_BP=10

 LEVEL_BP_OUT="rimin="${LEVEL_R_BP}"/255:gimin="${LEVEL_G_BP}"/255:bimin="${LEVEL_B_BP}"/255"

# white point
 IN_R_MAX=230
 IN_G_MAX=230
 IN_B_MAX=230

 OUT_R_MAX=255
 OUT_G_MAX=255
 OUT_B_MAX=255

 LEVEL_WP_IN="rimax="${IN_R_MAX}"/255:gimax="${IN_G_MAX}"/255:bimax="${IN_B_MAX}"/255"
 LEVEL_WP_OUT="romax="${OUT_R_MAX}"/255:gomax="${OUT_G_MAX}"/255:bomax="${OUT_B_MAX}"/255"

# actual level filter. don't forget to enable if you need it!!!
# LEVELS="colorlevels="${LEVEL_BP_OUT}":"${LEVEL_WP_IN}":"${LEVEL_WP_OUT}","
# :"${LEVEL_BP_IN}"


 EQ_CONTRAST="1.05"
 EQ_SATURATION="1.05"
 EQ_GAMMA="1.05"
 
 EQ="eq=contrast=${EQ_CONTRAST}:saturation=${EQ_SATURATION}:gamma=${EQ_GAMMA},"


 # back-to reversed-normal
# REV="reverse,split[b],reverse,[b]concat,"

 # normal[b]-reversed[src]  ([b] + src)
 REV="split[b],reverse,[b]concat,"

# REV="reverse,"

# REV="split[b],reverse,[b]concat,split=2[c][d],[c][d]concat=n=2, "

# FIX_DURATION="setpts=(PTS-STARTPTS)*(15/16.17),"

# FINAL_TRIM_15="trim=end=15,setpts=PTS-STARTPTS,"

# scale only if original is larger than desired resolution
# POST_SCALE="scale=1080:1080,"    # scale to 1080
# POST_SCALE="scale=1280:1280,"    # scale to ETSY max resolution for videos is 1280x1280

 # usual value for all parameters is bt709
 # for 4k x 60fps -> space, primaries, matrix -> bt.601 (PAL or NTSC) see below

# check stream information before setting primaries
# ffprobe -v error -show_streams .mp4 | grep color_

# SD Video      -> bt709
# HD Video      -> 
#       bt.601 PAL    -> bt470bg    # I read suggestion to use -color_trc gamma28, but usually is better to stick to video trc
#       bt.601 NTSC   -> smpte170m

 COLOR_PRIMARIES="bt709"
 COLOR_TRC="bt709"              # usual transfer for modern bt470bg but please check using command above
 COLOR_SPACE="bt709"
 COLOR_MATRIX="bt709"
# COLOR_RANGE="-color_range 2"  # 2 - full ?
# CHROMA_LOCATION="-chroma_location left"

 PIXEL_FORMAT="yuv420p"

# color matrix correction. comment out both lines to disable
 COLOR_MATRIX_VF="scale=out_color_matrix=${COLOR_MATRIX},"
 COLOR_ATTRS="-color_primaries ${COLOR_PRIMARIES} -color_trc ${COLOR_TRC} -colorspace ${COLOR_SPACE} ${CHROMA_LOCATION} ${COLOR_RANGE}"


 FILTER="${ALL_TRIM} ${FPS_30} ${PRE_SCALE} ${CROP} ${TRANSP} ${VFLIP} ${HFLIP} ${LEVELS} ${EQ} ${COLOR_MATRIX_VF} ${MAKE_CLIPS} ${REV} ${FIX_DURATION} ${FINAL_TRIM_15} ${POST_SCALE} format=${PIXEL_FORMAT}"

# require testing
 CODEC_COLOR_PARAMS="-x264opts colorprim=${COLOR_PRIMARIES}:transfer=${COLOR_TRC}:colormatrix=${COLOR_MATRIX}"

 CODEC_ATTRS="-codec:v libx264 ${CODEC_COLOR_PARAMS} -pix_fmt ${PIXEL_FORMAT} -preset:v slow -profile:v baseline -crf 20"

 EXPERIMENTAL_ATTRS="-movflags +write_colr"  # "" # -color_range tv -level 8
 EXPERIMENTAL_BITSTREAM_METADATA_ATTRS="" # "-bsf:v h264_metadata=video_full_range_flag=0"

# echo "${FILTER}"; exit

 INPUT_FILE="v1.mp4"
 OUTPUT_FILE="e1.mp4"

if [ -n "$1" ]; then
  INPUT_FILE="$1"
fi

if [ -n "$2" ]; then
  OUTPUT_FILE="$2"
fi

 TEMP_FILE="temp_${OUTPUT_FILE}"

set -eu
# ffmpeg -i "${INPUT_FILE}" -an -vf "${FILTER}" -codec:v libx264 -y "${OUTPUT_FILE}"

 ffmpeg -i "${INPUT_FILE}" -an -vf "${FILTER}" "${CODEC_ATTRS}" "${COLOR_ATTRS}" "${EXPERIMENTAL_ATTRS}" -y "${TEMP_FILE}"
 ffmpeg -i "${TEMP_FILE}" -metadata:s:0 rotate=180 "${COLOR_ATTRS}" "${EXPERIMENTAL_BITSTREAM_METADATA_ATTRS}" -c copy -y "${OUTPUT_FILE}"
 rm -f "${TEMP_FILE}"

#!/bin/bash

#    Script to cut audio ads from the video files and replace them with the original audio.
#    Copyight (C) 2022 - Stanislav Grinkov
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

prefix=ad_free
dest_audio_ext="aac"

transl_stream_id=1
orig_stream_id=2

usage() {
cat <<EOF

Usage:
  $0 video.ext ad1start ad1duration ad2start ad2duration
    uses ffmpeg to split, mix, and merge audio streams to remove audio ads
    result will be written as $prefix_video.mkv
    it reconverts audio stream as wav and then back to $dest_audio_ext
    examine script to tune the parameters for your needs

    source video file must have at least two audio streams.
    
    known limitations. ad1start can't be 00:00:00. leads to incorrect behavior

    example:
      $0 video.mkv 00:01:00 20 00:56:50 20

EOF
}

if [ $# -le 1 ]; then
  usage
  exit 0
fi

set -euo pipefail

movie="$1"

ad1start="$2"
ad1duration="$3"
ad1end="$(date --utc -d "$ad1start UTC +$ad1duration seconds" +%H:%M:%S)"

ad2start="$4"
ad2duration="$5"
ad2end="$(date --utc -d "$ad2start UTC +$ad2duration seconds" +%H:%M:%S)"

ad_free_audio=pt_ad_free.wav

merge_audio_list=merge_audio_list_file

execute() {
 ffmpeg -i "$1" -map 0:"$2" -ss "$3" -to "$4" -y "$5"
}

execute_last() {
 ffmpeg -i "$1" -map 0:"$2" -ss "$3" -y "$4"
}

write_merge_file() {
cat <<EOF >"$merge_audio_list"
file pt_0_start_to_ad1s.$ext
file pt_1_ad1s_to_ad1e.$ext
file pt_2_ad1e_to_ad2s.$ext
file pt_3_ad2s_to_ad2e.$ext
file pt_4_ad2e_to_end.$ext
EOF
}

merge_audio() {
  ffmpeg -f concat -i "$merge_audio_list" -y "$1"
  ffmpeg -i "$1" -b:a 192k "$1$dest_audio_ext"
}

merge_back() {
  ffmpeg -i "$1" -i "$2" -map "0:0" -map "1:0" -map "0:$3" -c:a copy -c:v copy -y "$4"
}

write_merge_file

execute "$movie" "$transl_stream_id" "00:00:00" "$ad1start" "pt_0_start_to_ad1s.$ext"
execute "$movie" "$orig_stream_id" "$ad1start" "$ad1end" "pt_1_ad1s_to_ad1e.$ext"
execute "$movie" "$transl_stream_id" "$ad1end" "$ad2start" "pt_2_ad1e_to_ad2s.$ext"
execute "$movie" "$orig_stream_id" "$ad2start" "$ad2end" "pt_3_ad2s_to_ad2e.$ext"
execute_last "$movie" "$transl_stream_id" "$ad2end" "pt_4_ad2e_to_end.$ext"

merge_audio "$ad_free_audio"

merge_back "$movie" "$ad_free_audio$dest_audio_ext" "$orig_stream_id" "$prefix"_"$movie"

rm -f "$ad_free_audio"
rm -f "$ad_free_audio$dest_audio_ext"
rm -f "pt_0_start_to_ad1s.$ext"
rm -f "pt_1_ad1s_to_ad1e.$ext"
rm -f "pt_2_ad1e_to_ad2s.$ext"
rm -f "pt_3_ad2s_to_ad2e.$ext"
rm -f "pt_4_ad2e_to_end.$ext"
rm -f "$merge_audio_list"

#! /bin/sh

#    Duplicate webcam video streams for jitsi and obs.
#    Copyight (C) 2022 - Stanislav Grinkov
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

# Access to webcam video stream is exclusive, so lets duplicate them. One for Jitsi, another one for OBS.
sudo modprobe -v v4l2loopback devices=2
gst-launch-1.0 v4l2src device=/dev/video0 ! tee name=t ! queue ! v4l2sink device=/dev/video2 t. ! queue ! v4l2sink device=/dev/video3

# If you have multi-monitor setup (but one huge desktop) you might want to share just a portion of it
PART_SIZE=1280x720
OFFSET=32,0 # x,y. Depending on your desktop/wm you might need to update the value

ffmpeg -f x11grab \
  -framerate 15 \
  -video_size "${PART_SIZE}" \
  -i :0.0+"${OFFSET}" \
  -show_region 1 -region_border 2 \
  -c:v bmp -f rawvideo -an pipe:1 | ffplay -i pipe:0

# Mmmm. Not sure what job I should kill and why. ffmpeg? gst? Need to test.
kill %1

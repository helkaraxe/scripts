#! /bin/sh

#    Source code for batch image resizing script using magick (ImageMagick 6 or 7)
#    Copyight (C) 2022 - Stanislav Grinkov
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

MAGICK="magick"

check_requirements() {
  BC=$(whereis bc)
  if [ ${#BC} -lt 4  ]; then # bc:
    echo "bc is not installed"
    exit 1
  fi
  
  MAG=$(whereis magick)
  if [ ${#MAG} -lt 8 ]; then
    # Imagemagick 7 is not found
    MAGICK=""
  fi
  
  if [ ${#MAGICK} -eq 0 ]; then
    # let's see maybe we have imagemagick 6
    MOGR=$(whereis mogrify) # mogrify is one of the IM6 tools
    if [ ${#MOGR} -lt 10 ]; then
      echo "ImageMagick not found. Please install at least version 6. I need 'identify' and 'convert' commands."
      exit 1
    fi
  fi
}

main() {
  set -eu
  desired_width="${1:-2000}"
  
  BAK=bak
  if [ ! -d  "${BAK}" ]; then
    mkdir "${BAK}"
  fi
  
  for f in *.jpg; do
    process_file "$f" "${desired_width}"
  done
  
  for f in *.JPG; do
    process_file "$f" "${desired_width}"
  done
}


process_file() {
  f="$1"
  desired_size="$2"

  if [ ! -e "${f}" ]; then
     exit
  fi
  
  if [ -f "${BAK}/${f}" ]; then 
    echo "skipped ${f}. Exists in backup, probably already resized"
    exit
  fi
  
  cp "${f}" "${BAK}/${f}"

  width="$(${MAGICK} identify -format "%w" "${f}")"
  height="$(${MAGICK} identify -format "%h" "${f}")"

  already_resized="$(echo "(${height} == ${desired_size} || ${width} == ${desired_size})" | bc )"
  if [ "${already_resized}" -eq 1 ]; then
    echo "skipped ${f}. Already required size.";
  fi

  height_is_taller="$(echo " ${height} >= ${width} " | bc )"
  if [ "${height_is_taller}" -eq 1 ]; then
    size="${desired_size}x" # image is tall, constraint width
  else
    size="x${desired_size}" # image is wide, constraint height
  fi

  ${MAGICK} convert "${f}" -resize "${size}" -quality 70 "${f}"
  res_width="$(${MAGICK} identify -format "%w" "${f}")"
  res_height="$(${MAGICK} identify -format "%h" "${f}")"
  echo "resized ${f} to ${res_width}x${res_height} (was ${width}x${height})"
}

check_requirements
main "$1"
